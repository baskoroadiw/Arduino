#include <LiquidCrystal_SR_LCD3.h>

const int PIN_LCD_STROBE = 2;
const int PIN_LCD_DATA = 4;
const int PIN_LCD_CLOCK = 3;
int pingPin = 13;
int echoPin = 12;
int buz = A5;

int button = 0;
String sBarisAtas = "O   X   O   X   X   X   O   O                   ";
String sBarisBawah = "  O   O   X   O   X   X   O   X                 ";

char barisAtas, barisBawah;
long duration,cm;

int a=11;
int b=10;
int c=9;
int d=8;
int e=7;
int f=6;
int g=5;

LiquidCrystal_SR_LCD3 lcd(PIN_LCD_DATA, PIN_LCD_CLOCK, PIN_LCD_STROBE);


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  lcd.begin(16,2);
  lcd.setCursor(0,0);
  lcd.println("Nama:Baskoro Adi");
  lcd.setCursor(0,1);
  lcd.println("NIM:6706174071");

  pinMode(pingPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(buz, OUTPUT);

  for(int i=11; i>=5; i--){
   pinMode(i,OUTPUT);
  }

  for (int i=9;i>=0;i--){
    sevenSegmen(i);
    delay(600);
  }
      lcd.clear();
      lcd.println("Press space to");
      lcd.setCursor(0,1);
      lcd.println("run the game");
  
      digitalWrite(buz, HIGH);
      delay(1500);
      digitalWrite(buz, LOW);
}

void loop() {
  // put your main code here, to run repeatedly:
  if(Serial.available() > 0){
    button = Serial.read() - 48;
    lcd.clear();
    
    for (int i=0;i<=47;i++){
      if(i>=16){
        lcd.scrollDisplayRight();
      }
      digitalWrite(pingPin, LOW);
      delayMicroseconds(2);
      digitalWrite(pingPin, HIGH);
      delayMicroseconds(10);
      digitalWrite(pingPin, LOW);
      
      duration = pulseIn(echoPin, HIGH); 
      cm = microsecondsToCentimeters(duration); 
    
        barisAtas = sBarisAtas[i];
        delay(cm);
        lcd.setCursor(i,0);
        lcd.print(barisAtas);
        Serial.println("Isi Baris Atas: ");

        lcd.setCursor(i,1);
        barisBawah = sBarisBawah[i];
        delay(cm);
        lcd.print(barisBawah);
        Serial.println("Isi Baris Bawah: ");
    }
  }
}

long microsecondsToCentimeters(long microseconds){ 
  return microseconds /29 / 2;
}

void sevenSegmen(int angka){
  switch(angka){
    case 1:
      digitalWrite(a,LOW);
      digitalWrite(b,HIGH);
      digitalWrite(c,HIGH);
      digitalWrite(d,LOW);
      digitalWrite(e,LOW);
      digitalWrite(f,LOW);
      digitalWrite(g,LOW);
      break;

    case 2:
      digitalWrite(a,HIGH);
      digitalWrite(b,HIGH);
      digitalWrite(c,LOW);
      digitalWrite(d,HIGH);
      digitalWrite(e,HIGH);
      digitalWrite(f,LOW);
      digitalWrite(g,HIGH);
      break;

    case 3:
      digitalWrite(a,HIGH);
      digitalWrite(b,HIGH);
      digitalWrite(c,HIGH);
      digitalWrite(d,HIGH);
      digitalWrite(e,LOW);
      digitalWrite(f,LOW);
      digitalWrite(g,HIGH);
      break;

    case 4:
      digitalWrite(a,LOW);
      digitalWrite(b,HIGH);
      digitalWrite(c,HIGH);
      digitalWrite(d,LOW);
      digitalWrite(e,LOW);
      digitalWrite(f,HIGH);
      digitalWrite(g,HIGH);
      break;

    case 5:
      digitalWrite(a,HIGH);
      digitalWrite(b,LOW);
      digitalWrite(c,HIGH);
      digitalWrite(d,HIGH);
      digitalWrite(e,LOW);
      digitalWrite(f,HIGH);
      digitalWrite(g,HIGH);
      break;

    case 6:
      digitalWrite(a,HIGH);
      digitalWrite(b,LOW);
      digitalWrite(c,HIGH);
      digitalWrite(d,HIGH);
      digitalWrite(e,HIGH);
      digitalWrite(f,HIGH);
      digitalWrite(g,HIGH);
      break;

    case 7:
      digitalWrite(a,HIGH);
      digitalWrite(b,HIGH);
      digitalWrite(c,HIGH);
      digitalWrite(d,LOW);
      digitalWrite(e,LOW);
      digitalWrite(f,LOW);
      digitalWrite(g,LOW);
      break;

    case 8:
      digitalWrite(a,HIGH);
      digitalWrite(b,HIGH);
      digitalWrite(c,HIGH);
      digitalWrite(d,HIGH);
      digitalWrite(e,HIGH);
      digitalWrite(f,HIGH);
      digitalWrite(g,HIGH);
      break;

    case 9:
      digitalWrite(a,HIGH);
      digitalWrite(b,HIGH);
      digitalWrite(c,HIGH);
      digitalWrite(d,HIGH);
      digitalWrite(e,LOW);
      digitalWrite(f,HIGH);
      digitalWrite(g,HIGH);
      break;

    case 0:
      digitalWrite(a,HIGH);
      digitalWrite(b,HIGH);
      digitalWrite(c,HIGH);
      digitalWrite(d,HIGH);
      digitalWrite(e,HIGH);
      digitalWrite(f,HIGH);
      digitalWrite(g,LOW);
      break;
  }

}
